package net.guillaume.teaching.refactoring.monopoly;

public class CaseConstructible extends Case {

	private final int coutAchat;
	private final int coutLoyer;
	private Proprietaire proprietaire;

	public CaseConstructible(String name, int coutAchat, int coutLoyer) {
		super(name);
		this.coutAchat = coutAchat;
		this.coutLoyer = coutLoyer;
		this.proprietaire = null;
	}

	public int getCoutAchat() {
		return coutAchat;
	}

	public int getCoutLoyer() {
		return coutLoyer;
	}

	public boolean isEnPropriete() {
		return (proprietaire != null);
	}

	public Proprietaire getProprietaire() {
		return proprietaire;
	}

	public void devientProprietaire(Joueur joueur) {
		proprietaire = new Proprietaire(joueur);
	}
	
    protected CaseConstructibleGroup getCaseGroup() {
		return (CaseConstructibleGroup) super.getCaseGroup();
	}
    
    public boolean hasSimilaireProprietaire(Proprietaire proprietaire)
    {
    	return proprietaire.equals(getProprietaire());
    }
}
