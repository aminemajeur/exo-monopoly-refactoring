package net.guillaume.teaching.refactoring.monopoly;


public interface IAction {

    void executer(Joueur joueur);
}
