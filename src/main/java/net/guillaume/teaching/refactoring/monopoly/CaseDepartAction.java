package net.guillaume.teaching.refactoring.monopoly;

public class CaseDepartAction implements IAction {

	private static final int BONUS_DE_NOUVELLE_TOUR = 200;

	@Override
	public void executer(Joueur joueur) {
		joueur.augmenterArgent(BONUS_DE_NOUVELLE_TOUR);
		joueur.incrementerNombreTour();
		if (joueur.isEnPrison() && joueur.isTerminerNombreTourMaxEnPrison()) {
			joueur.delibererPrison();
		}
	}

}
