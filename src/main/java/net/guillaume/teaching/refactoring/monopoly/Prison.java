package net.guillaume.teaching.refactoring.monopoly;

public class Prison {

	private boolean active;
	private int tourEntree;

	private static final int NOMBRE_TOURS_DELIBERATION = 3;

	public boolean isActive() {
		return active;
	}

	public boolean isExceedNombreToursMax(int tourEncours) {
		return (tourEncours - tourEntree) == NOMBRE_TOURS_DELIBERATION;
	}

	public void activer(int tourEncours) {
		active = true;
		tourEntree = tourEncours;
	}

	public void desactiver() {
		active = false;
		tourEntree = -1;
	}

}
