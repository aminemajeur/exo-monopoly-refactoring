package net.guillaume.teaching.refactoring.monopoly;

public class CaseContsructibleAction implements IAction{

	@Override
	public void executer(Joueur joueur) {
		CaseConstructible caseConstructible = (CaseConstructible) joueur.getCaseEncour();
		if(!caseConstructible.isEnPropriete()){
			joueur.acheterCaseConstructible(caseConstructible);
		}
		else{
			Proprietaire proprietaire = caseConstructible.getProprietaire();
			if(!caseConstructible.getCaseGroup().isEnPropriete(proprietaire)){
				proprietaire.loyer(caseConstructible.getCoutLoyer());
				joueur.loyer(caseConstructible.getCoutLoyer());
			}else
			{
				proprietaire.loyer(caseConstructible.getCoutLoyer() * 2);
				joueur.loyer(caseConstructible.getCoutLoyer() * 2);
			}
			
		}
	}

}
