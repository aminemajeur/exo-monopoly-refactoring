package net.guillaume.teaching.refactoring.monopoly;

import java.util.List;

public class CaseConstructibleGroup extends AbstractCaseGroup {

	public CaseConstructibleGroup(String name, int size, Couleur couleur) {
		super(name, size, couleur);
	}

	public boolean isEnPropriete(Proprietaire proprietaire) {
		boolean enPropriete = true;
		for (CaseConstructible caseConstructible : getCases()) {
			enPropriete &= caseConstructible.isEnPropriete()
					& caseConstructible.hasSimilaireProprietaire(proprietaire);
		}
		return enPropriete;
	}
	
	public int getNombreCaseEnPropriete(Proprietaire proprietaire) {
		int nombreCaseEnPropriete = 0;
		for (CaseConstructible caseConstructible : getCases()) {
			if(caseConstructible.isEnPropriete()
					& caseConstructible.hasSimilaireProprietaire(proprietaire))
				nombreCaseEnPropriete++;
		}
		return nombreCaseEnPropriete;
	}

	@Override
	protected List<CaseConstructible> getCases() {
		return (List<CaseConstructible>) super.getCases();
	}

}
