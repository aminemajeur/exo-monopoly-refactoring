package net.guillaume.teaching.refactoring.monopoly;

import java.util.List;

public class JoueurManager {

	private List<Joueur> joueurs;

	public void ajouter(Joueur joueur) {
		joueurs.add(joueur);
	}

	public void ecarter(Joueur joueur) {
		joueurs.remove(joueur);
	}

	public void initialiserJoueurs(final List<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public void lancerJoueurs(List<Case> casesPlatau) {
		for (Joueur joueur : joueurs) {
			De de = new De();
			avancerJoueur(joueur, casesPlatau, de);
		}
	}

	private void avancerJoueur(Joueur joueur, List<Case> cases, De de) {
		de.random();
		if (!de.isDouble() && !joueur.isEnPrison()) {
			de.initialiser();
			mouverJoueur(joueur, de.getTotal());
			for (Case cas : cases) {
				cas.getAction().executer(joueur);
			}
		} else {
			if (!joueur.isEnPrison()) {
				de.sommeDouble();
				while (!de.isExceedNombreDoubleMax()) {
					avancerJoueur(joueur, cases, de);
				}
				if (de.isExceedNombreDoubleMax()){
					new CasePrisonAction().executer(joueur);
					de.initialiser();
				}
			}else{
				joueur.delibererPrison();
				avancerJoueur(joueur, cases, de);
			}

		}
	}

	private void mouverJoueur(Joueur joueur, int pos) {
		for (int i = 0; i < pos; i++) {
			Case cas = joueur.mouver();
			if (cas.equals(CaseDepart.class)) {
				cas.getAction().executer(joueur);
			}
		}
	}
	
}
