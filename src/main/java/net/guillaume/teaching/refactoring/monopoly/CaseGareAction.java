package net.guillaume.teaching.refactoring.monopoly;

public class CaseGareAction implements IAction {

	@Override
	public void executer(Joueur joueur) {
		CaseConstructible caseConstructible = (CaseConstructible) joueur
				.getCaseEncour();
		if (!caseConstructible.isEnPropriete()) {
			joueur.acheterCaseConstructible(caseConstructible);
		} else {
			Proprietaire proprietaire = caseConstructible.getProprietaire();
			proprietaire.loyer(caseConstructible.getCoutLoyer()
					* caseConstructible.getCaseGroup()
							.getNombreCaseEnPropriete(proprietaire));
			joueur.loyer(caseConstructible.getCoutLoyer()
					* caseConstructible.getCaseGroup()
							.getNombreCaseEnPropriete(proprietaire));
		}
	}

}
