package net.guillaume.teaching.refactoring.monopoly;

public class Joueur implements Comparable {

	private final String nom;
	private final char sexe;
	private int argent;
	private Case caseEncour;
	private int tourEncours;
	private boolean enPrison;
	private boolean enFallite;
	private Prison prison;

	public Joueur(String nom, char sexe, Case caseCourante) {
		this.nom = nom;
		this.sexe = sexe;
		argent = 400;
		tourEncours = 0;
		enPrison = false;
		this.caseEncour = caseCourante;
		prison = new Prison();
		enFallite = false;
	}

	public Case getCaseEncour() {
		return caseEncour;
	}

	public int getArgent() {
		return argent;
	}

	public String getNom() {
		return nom;
	}

	public String getPrefixSexe() {
		return (sexe == 'M') ? "Il" : "Elle";
	}

	public char getSexe() {
		return sexe;
	}

	public boolean estEnPrison() {
		return enPrison;
	}

	public Prison getPrison() {
		return prison;
	}

	public void ouSuisJe() {
		if (argent > 0) {
			System.out.println("Sa position est " + caseEncour.getNom()
					+ ", son argent est : " + argent + ".");
		} else {
			System.out.println("Sa position est " + caseEncour.getNom() + ". "
					+ getSexe() + " n'a plus d'argent.");
		}

	}

	private void faireFailliteACauseLoyer() {
		enFallite = true;
		System.out.print(getNom()
				+ " ne peut pas payer le loyer en entier. Il fait faillite.");
	}

	public int compareTo(Object other) { // sert au classement fianl des joueurs
		int nombre1 = ((Joueur) other).getArgent();
		int nombre2 = this.getArgent();
		if (nombre1 > nombre2)
			return -1;
		else if (nombre1 == nombre2)
			return 0;
		else
			return 1;
	}

	public void acheterCaseConstructible(CaseConstructible caseConstructible) {
		if (caseConstructible.getCoutAchat() < argent) {
			diminuerArgent(caseConstructible.getCoutAchat());
			caseConstructible.devientProprietaire(this);
			System.out.println(" * " + nom + " achete "
					+ caseConstructible.getNom() + " *");
		}
	}

	public void augmenterArgent(int argent) {
		this.argent += argent;
	}

	public void diminuerArgent(int argent) {
		this.argent -= argent;
	}

	public void diminuerArgentParTaux(float taux) {
		this.argent = (int) Math.floor(this.argent * taux);
	}

	public void incrementerNombreTour() {
		tourEncours++;
	}

	public int getTourEncours() {
		return tourEncours;
	}

	public Case mouver() {
		return (caseEncour = caseEncour.getSuivante());
	}

	public void entrerPrison() {
		prison.activer(tourEncours);
	}

	public void delibererPrison() {
		prison.desactiver();
	}

	public boolean isTerminerNombreTourMaxEnPrison() {
		return prison.isExceedNombreToursMax(tourEncours);
	}

	public boolean isEnPrison() {
		return prison.isActive();
	}

	public void loyer(int coutLoye) {
		if (coutLoye < argent)
			diminuerArgent(coutLoye);
		else {
			faireFailliteACauseLoyer();
		}
	}

	public boolean estEnFaillite() {
		return enFallite;
	}
}
