package net.guillaume.teaching.refactoring.monopoly;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.guillaume.teaching.refactoring.monopoly.CaseConstructible;
import net.guillaume.teaching.refactoring.monopoly.AbstractCaseGroup.Couleur;

public class Plateau {

	private Case depart;
	private Case bellevile;
	private Case lecourbe;
	private Case vaugirard;
	private Case courcelles;
	private Case republique;
	private Case neuilly;
	private Case villette;
	private Case paradis;
	private Case mozart;
	private Case stmichel;
	private Case pigalle;
	private Case malesherbes;
	private Case matignon;
	private Case henrimartin;
	private Case bourse;
	private Case sthonore;
	private Case fayette;
	private Case foch;
	private Case breteuil;
	private Case capucines;
	private Case champs;
	private Case paix;
	private Case luxe;
	private Case impot;
	private Case prison;
	private Case allerenprison;
	private Case garemontparnasse;
	private Case garedelyon;
	private Case garedunord;
	private Case garesaintlazard;

	AbstractCaseGroup caseConstructibleGroupVert;
	AbstractCaseGroup caseConstructibleGroupBleu;
	AbstractCaseGroup caseConstructibleGroupRose;
	AbstractCaseGroup caseConstructibleGroupOrange;
	AbstractCaseGroup caseConstructibleGroupRouge;
	AbstractCaseGroup caseConstructibleGroupJaune;
	AbstractCaseGroup caseConstructibleGroupMaron;
	AbstractCaseGroup caseConstructibleGroupBleuLeger;
	AbstractCaseGroup caseGareGroup;

	private List<Case> cases = new LinkedList<Case>();

	private Plateau() {
		creerCases();
		creerGroupeCases();
		associeGroupeCases();
		bindCases();
	}

	public static Plateau create() {
		return new Plateau();
	}

	private void creerGroupeCases() {

		caseConstructibleGroupVert = new CaseConstructibleGroup("VERT", 2,
				Couleur.VERT);
		caseConstructibleGroupVert.ajouter(bellevile);
		caseConstructibleGroupVert.ajouter(lecourbe);

		caseConstructibleGroupBleu = new CaseConstructibleGroup("BLEU", 3,
				Couleur.BLEU);

		caseConstructibleGroupBleu.ajouter(vaugirard);
		caseConstructibleGroupBleu.ajouter(courcelles);
		caseConstructibleGroupBleu.ajouter(republique);

		caseConstructibleGroupRose = new CaseConstructibleGroup("ROSE", 3,
				Couleur.ROSE);

		caseConstructibleGroupRose.ajouter(neuilly);
		caseConstructibleGroupRose.ajouter(villette);
		caseConstructibleGroupRose.ajouter(paradis);

		caseConstructibleGroupOrange = new CaseConstructibleGroup("ORANGE", 3,
				Couleur.ORANGE);

		caseConstructibleGroupOrange.ajouter(mozart);
		caseConstructibleGroupOrange.ajouter(stmichel);
		caseConstructibleGroupOrange.ajouter(pigalle);

		caseConstructibleGroupRouge = new CaseConstructibleGroup("ROUGE", 3,
				Couleur.ROUGE);

		caseConstructibleGroupRouge.ajouter(malesherbes);
		caseConstructibleGroupRouge.ajouter(matignon);
		caseConstructibleGroupRouge.ajouter(henrimartin);

		caseConstructibleGroupJaune = new CaseConstructibleGroup("JAUNE", 3,
				Couleur.JAUNE);

		caseConstructibleGroupJaune.ajouter(bourse);
		caseConstructibleGroupJaune.ajouter(sthonore);
		caseConstructibleGroupJaune.ajouter(fayette);

		caseConstructibleGroupMaron = new CaseConstructibleGroup("MARON", 3,
				Couleur.MARON);

		caseConstructibleGroupMaron.ajouter(foch);
		caseConstructibleGroupMaron.ajouter(breteuil);
		caseConstructibleGroupMaron.ajouter(capucines);

		caseConstructibleGroupBleuLeger = new CaseConstructibleGroup(
				"BlEULEGER", 2, Couleur.BlEULEGER);

		caseConstructibleGroupBleuLeger.ajouter(champs);
		caseConstructibleGroupBleuLeger.ajouter(paix);

		caseGareGroup = new CaseConstructibleGroup("Gare", 2, Couleur.NONE);
		caseGareGroup.ajouter(garedelyon);
		caseGareGroup.ajouter(garemontparnasse);
		caseGareGroup.ajouter(garedunord);
		caseGareGroup.ajouter(garesaintlazard);
	}

	private void creerCases() {

		depart = new CaseDepart("Depart");
		bellevile = new CaseConstructible("Boulevard de Bellevile", 20, 2);
		lecourbe = new CaseConstructible("Rue Lecourbe", 40, 4);
		vaugirard = new CaseConstructible("Rue Vaugirard", 60, 6);
		courcelles = new CaseConstructible("Rue De Courcelles", 60, 6);
		republique = new CaseConstructible("Avenue de la Republique", 80, 8);
		neuilly = new CaseConstructible("Avenue de Neuilly", 100, 10);
		villette = new CaseConstructible("Boulevard de la Villette", 100, 10);
		paradis = new CaseConstructible("Rue de Paradis", 120, 12);
		mozart = new CaseConstructible("Avenue Mozart", 140, 14);
		cases.add(mozart);
		stmichel = new CaseConstructible("Boulevard St Michel", 140, 14);
		cases.add(stmichel);
		pigalle = new CaseConstructible("Place Pigalle", 160, 16);
		cases.add(pigalle);
		malesherbes = new CaseConstructible("Boulevard Malesherbes", 180, 18);
		cases.add(malesherbes);
		matignon = new CaseConstructible("Avenue Matignon", 180, 18);
		cases.add(matignon);
		henrimartin = new CaseConstructible("Avenue Henri Martin", 200, 20);
		cases.add(henrimartin);
		bourse = new CaseConstructible("Place de la Bourse", 220, 22);
		cases.add(bourse);
		sthonore = new CaseConstructible("Faubourg St Honore", 220, 20);
		cases.add(sthonore);
		fayette = new CaseConstructible("Rue de la Fayette", 240, 24);
		cases.add(fayette);
		foch = new CaseConstructible("Avenue Foch", 260, 26);
		cases.add(foch);
		breteuil = new CaseConstructible("Avenue de Breteuil", 260, 26);
		cases.add(breteuil);
		capucines = new CaseConstructible("Boulevard des Capucines", 280, 28);
		cases.add(capucines);
		champs = new CaseConstructible("Avenue des Champs Elysees", 300, 30);
		cases.add(champs);
		paix = new CaseConstructible("Rue de la paix", 350, 35);
		cases.add(paix);
		impot = new CaseImpot("Impot sur le Revenu");
		cases.add(impot);
		luxe = new CaseImpot("Taxe de Luxe");
		cases.add(luxe);
		prison = new CasePrison("Prison");
		cases.add(prison);
		allerenprison = new CasePrison("Aller en Prison");
		cases.add(allerenprison);
		garedelyon = new CaseConstructible("Gare De Lyon", 120, 25);
		cases.add(garedelyon);
		garemontparnasse = new CaseConstructible("Gare Mont-Parnasse", 120, 25);
		cases.add(garemontparnasse);
		garedunord = new CaseConstructible("Gare Du Nord", 120, 25);
		cases.add(garedunord);
		garesaintlazard = new CaseConstructible("Gare Saint-Lazar", 120, 25);
		cases.add(garesaintlazard);
	}

	private void associeCasesPlateau(){
		cases.add(depart);
		cases.add(bellevile);
		cases.add(lecourbe);
		cases.add(vaugirard);
		cases.add(courcelles);
		cases.add(republique);		
		cases.add(neuilly);
		cases.add(villette);
		cases.add(paradis);
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	private void associeGroupeCases() {

		bellevile.setCaseGroup(caseConstructibleGroupVert);
		lecourbe.setCaseGroup(caseConstructibleGroupVert);
		vaugirard.setCaseGroup(caseConstructibleGroupBleu);
		courcelles.setCaseGroup(caseConstructibleGroupBleu);
		republique.setCaseGroup(caseConstructibleGroupBleu);
		neuilly.setCaseGroup(caseConstructibleGroupRose);
		villette.setCaseGroup(caseConstructibleGroupRose);
		paradis.setCaseGroup(caseConstructibleGroupRose);
		mozart.setCaseGroup(caseConstructibleGroupOrange);
		stmichel.setCaseGroup(caseConstructibleGroupOrange);
		pigalle.setCaseGroup(caseConstructibleGroupOrange);
		malesherbes.setCaseGroup(caseConstructibleGroupRouge);
		matignon.setCaseGroup(caseConstructibleGroupRouge);
		henrimartin.setCaseGroup(caseConstructibleGroupRouge);
		bourse.setCaseGroup(caseConstructibleGroupJaune);
		sthonore.setCaseGroup(caseConstructibleGroupJaune);
		fayette.setCaseGroup(caseConstructibleGroupJaune);
		foch.setCaseGroup(caseConstructibleGroupMaron);
		breteuil.setCaseGroup(caseConstructibleGroupMaron);
		capucines.setCaseGroup(caseConstructibleGroupMaron);
		champs.setCaseGroup(caseConstructibleGroupBleuLeger);
		paix.setCaseGroup(caseConstructibleGroupBleuLeger);
		garedelyon.setCaseGroup(caseGareGroup);
		garemontparnasse.setCaseGroup(caseGareGroup);
		garedunord.setCaseGroup(caseGareGroup);
		garesaintlazard.setCaseGroup(caseGareGroup);

	}

	private void bindCases() {
		depart.setSuivante(bellevile);
		bellevile.setSuivante(lecourbe);
		lecourbe.setSuivante(impot);
		impot.setSuivante(garemontparnasse);
		garemontparnasse.setSuivante(vaugirard);
		vaugirard.setSuivante(courcelles);
		courcelles.setSuivante(republique);
		republique.setSuivante(prison);
		prison.setSuivante(villette);
		villette.setSuivante(neuilly);
		neuilly.setSuivante(paradis);
		paradis.setSuivante(garedelyon);
		garedelyon.setSuivante(mozart);
		mozart.setSuivante(stmichel);
		stmichel.setSuivante(pigalle);
		pigalle.setSuivante(matignon);
		matignon.setSuivante(malesherbes);
		malesherbes.setSuivante(henrimartin);
		henrimartin.setSuivante(garedunord);
		garedunord.setSuivante(sthonore);
		sthonore.setSuivante(bourse);
		bourse.setSuivante(fayette);
		fayette.setSuivante(allerenprison);
		allerenprison.setSuivante(breteuil);
		breteuil.setSuivante(foch);
		foch.setSuivante(capucines);
		capucines.setSuivante(garesaintlazard);
		garesaintlazard.setSuivante(champs);
		champs.setSuivante(luxe);
		luxe.setSuivante(paix);
		paix.setSuivante(depart);
	}

}
