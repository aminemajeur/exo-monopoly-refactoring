package net.guillaume.teaching.refactoring.monopoly;

public class CasePrisonAction implements IAction {

	@Override
	public void executer(Joueur joueur) {
		joueur.entrerPrison();
	}

}
