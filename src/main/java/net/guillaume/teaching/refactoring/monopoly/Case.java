package net.guillaume.teaching.refactoring.monopoly;

public abstract class Case{

    private final String nom;
    private Case suivante;
    
    private IAction action;
    
    private AbstractCaseGroup caseGroup;
    
    private JoueurManager joueurManager;

    public Case(String nom) {
        this.nom = nom;
    }

    public void setSuivante(Case c) {
        suivante = c;
    }

    public Case getSuivante() {
        return suivante;
    }

    public String getNom() {
        return nom;
    }
    
    protected IAction getAction() {
		return action;
	}
    
    protected void setAction(IAction action) {
		this.action = action;
	}

    public void setJoueurManager(JoueurManager joueurManager) {
		this.joueurManager = joueurManager;
	}
    
    protected void setCaseGroup(AbstractCaseGroup caseGroup) {
		this.caseGroup = caseGroup;
	}
    
    protected AbstractCaseGroup getCaseGroup() {
		return caseGroup;
	}
}
