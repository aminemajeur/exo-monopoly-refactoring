package net.guillaume.teaching.refactoring.monopoly;

public class De {

	private int valeurDe1;
	private int valeurDe2;
	private int nombreDouble;

	private static final int NOMBRE_DOUBLE_MAX = 3;

	public De random() {
		valeurDe1 = (int) (Math.random() * 6) + 1;
		valeurDe2 = (int) (Math.random() * 6) + 1;
		return this;
	}

	public void sommeDouble() {
		nombreDouble++;
	}

	public boolean isExceedNombreDoubleMax() {
		return nombreDouble == NOMBRE_DOUBLE_MAX;
	}

	public void initialiser() {
		nombreDouble = 0;
	}

	public int getTotal() {
		return valeurDe1 + valeurDe2;
	}

	public boolean isDouble() {
		return valeurDe1 == valeurDe2;
	}

}