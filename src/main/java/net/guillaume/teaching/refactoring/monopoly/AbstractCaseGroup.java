package net.guillaume.teaching.refactoring.monopoly;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCaseGroup {

	enum Couleur {
		NONE, MARON, BlEULEGER, ROSE, ORANGE, ROUGE, JAUNE, VERT, BLEU
	}

	private final String name;
	private final int size;
	private final Couleur couleur;
	private List<Case> cases;

	public AbstractCaseGroup(String name, int size, Couleur couleur) {
		this.name = name;
		this.cases = new ArrayList<Case>();
		this.size = size;
		this.couleur = couleur;
	}

	public void ajouter(Case cas) {
		cases.add(cas);
	}

	public int taille() {
		return size;
	}
	
	protected List<? extends Case> getCases() {
		return cases;
	}
	
	public Couleur getCouleur() {
		return couleur;
	}
	
	public String getName() {
		return name;
	}
}
