package net.guillaume.teaching.refactoring.monopoly;

public class Proprietaire extends Joueur{
	
	

	public Proprietaire(String nom, char sexe, Case caseCourante) {
		super(nom, sexe, caseCourante);
	}
	
	public Proprietaire(Joueur joueur) {
		super(joueur.getNom(), joueur.getSexe(),joueur.getCaseEncour());
	}
	
	public void loyer(int coutLoye){
		augmenterArgent(coutLoye);
	}
	
	public void vendre(){}

}
