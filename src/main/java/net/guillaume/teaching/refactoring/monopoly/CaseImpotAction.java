package net.guillaume.teaching.refactoring.monopoly;

public class CaseImpotAction implements IAction {

	private static final float TAU_IMPOT = 0.9f;

	@Override
	public void executer(Joueur joueur) {
		joueur.diminuerArgentParTaux(TAU_IMPOT);
	}

}
